
import 'package:flutter_cicd/main.dart';
import 'package:rxdart/rxdart.dart';
final emptyObject =  TestCompute(10, DateTime.now());
class ResetableBehaviorSubject<T>{
  final  _behaviorSubject = BehaviorSubject<dynamic>();
  void add(T t){
    if(!_behaviorSubject.isClosed){
      _behaviorSubject.add(t);
    }
  }
  void reset(){
    if(!_behaviorSubject.isClosed){
      _behaviorSubject.add(emptyObject);
    }
  }
  void close(){
    _behaviorSubject.close();
  }
}